package manager;

import models.User;
import securesocial.core.java.SecureSocial;

public class LoginManager {
	public boolean userExists() {
		if(findUserByEmailId(SecureSocial.currentUser().email().get()) == true) {

			//Do nothing
		}
		else
		{
			createUser();
		}
		return true;
	}
    
    private boolean findUserByEmailId(String email) {
    	
    	User user = User.usersByEmail(email);
    	if(user != null) {
    		return true;
    	}
    	return false;
    }
    
    private boolean createUser() {
    	
        User user = new User();
        
        user.user_name = SecureSocial.currentUser().identityId().userId();
        user.auth_provider = SecureSocial.currentUser().identityId().providerId();
        user.password = SecureSocial.currentUser().passwordInfo().toString();
        user.first_name = SecureSocial.currentUser().firstName();
        user.last_name = SecureSocial.currentUser().lastName();
        user.email_id = SecureSocial.currentUser().email().isDefined()? SecureSocial.currentUser().email().get() : "Not available";
        user.phoneno = "0000000000";
        
        user.save();
        
        return true;
   }
}
