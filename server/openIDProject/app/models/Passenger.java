package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="passenger")
public class Passenger extends Model {

  @Id
  @Column(name="user_name")
  public String user_name;
  public String credit_card_no;
  public String exp_date;
  public String cvv;
  public String pin;
  public Long total_miles_travelled;
  public Long total_bonus;
  
    
  public static Finder<Long,Passenger> find = new Finder<Long,Passenger>(Long.class, Passenger.class); 

}
