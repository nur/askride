name := "openIDProject"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "org.postgresql" % "postgresql" % "9.2-1003-jdbc4",
  "securesocial" %% "securesocial" % "2.1.2",
  "com.stripe" % "stripe-java" % "1.24.1",
  "commons-io" % "commons-io" % "2.3"
)

resolvers += Resolver.sonatypeRepo("snapshots")


play.Project.playJavaSettings

