package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Result;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;

import models.State;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import views.html.*;



public class ArUtil extends Controller{

	private static final int SUCCESS=0;
	private static final int FAILURE=1;
	final static String IMAGE_RESOURCE_PATH = "/res/images";

	public static Result getStatelist() {
		
		// Find all US State
		List<State> stateList = State.find.all();
		JsonNode allStateList = Json.toJson(stateList);
		
		ObjectNode result = Json.newObject();

		result.put("status", SUCCESS); //0-> Success    1->Failure
		result.put("allStateList", allStateList);

		return ok(result); 
	}
	
	public static Result uploadImage() throws IOException {
		ObjectNode result = Json.newObject();
		MultipartFormData body = request().body().asMultipartFormData();
		FilePart photo = body.getFile("file");
		if (photo != null) {
			String fileName = photo.getFilename();
			File file = photo.getFile();
			
			File directory = new File(play.Play.application().path().toString() + "//public//images//userData//" + "66666999999");

			if(!directory.exists()) {
				directory.mkdirs();
			}

			File newFile = new File(directory.getAbsolutePath() + "/UserImage.jpg");
			file.renameTo(newFile); //here you are moving photo to new directory   
			
			System.out.println(newFile.getPath()); //this path you can store in database

			result.put("status", SUCCESS); //0-> Success    1->Failure
			result.put("upload_status", "File uploaded");
		}
		else
		{
			result.put("status", FAILURE); //0-> Success    1->Failure
			result.put("upload_status", "File NOT uploaded");
		}
		return ok(result);	
	}

	public static Result getImage(String user_name) {
		try {
			
			String userImageFileLocation = play.Play.application().path().toString() + "//public//images//userData//66666999999//" + user_name + ".jpg" ;
			System.out.println("userImageFileLocation = " + userImageFileLocation);

			File file = new File(userImageFileLocation);
			
			if(!file.exists())
			{
				System.out.println("Error :: No image exists for user - " + user_name);
				return ok("No image exists for user : " + user_name);
			}
			
			BufferedImage image = ImageIO.read(file);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", baos);

			return ok(baos.toByteArray()).as("image/jpg");
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ok();
	}
	
//	public static Result upload() throws IOException {
//		ObjectNode result = Json.newObject();
//		MultipartFormData body = request().body().asMultipartFormData();
//		FilePart picture = body.getFile("file");
//
//		System.out.println("Point 1 --> P A L A K  .... ");
//		if (picture != null) {
//			// String fileName = picture.getFilename();
//			// String contentType = picture.getContentType(); 
//			File file = picture.getFile();
//
//			byte []image = new byte[(int) file.length()];
//			InputStream ios = null;
//			try {
//				ios = new FileInputStream(file);
//				if ( ios.read(image) == -1 ) {
//					throw new IOException("EOF reached while trying to read the whole file");
//				}        
//			} finally { 
//				try {
//					if ( ios != null ) 
//						ios.close();
//				} catch ( IOException e) {
//				}
//			}
//
//			// Uncomment below line. create Folder based on the user ID 
//			// File directory = new File(System.getProperty("user.home"), "userData/" + SecureSocial.currentUser().identityId().userId());
//			File directory = new File(System.getProperty("user.home"), "userData/" + "66666999999");
//
//			if(!directory.exists()) {
//				directory.mkdirs();
//			}
//
//			String uploadFileLocation = directory.getAbsolutePath() + "/UserImage.jpg" ;
//			System.out.println("uploadFileLocation = " + uploadFileLocation);
//			try {
//				OutputStream out = new FileOutputStream(new File(uploadFileLocation));
//				IOUtils.write(image, out);
//				out.flush();
//				out.close();
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			result.put("status", SUCCESS); //0-> Success    1->Failure
//			result.put("upload_status", "File uploaded");
//
//		} else {
//			flash("error", "Missing file");
//			result.put("status", FAILURE); //0-> Success    1->Failure
//			result.put("upload_status", "File NOT uploaded");
//		}
//		return ok(result);
//	}
	
}
