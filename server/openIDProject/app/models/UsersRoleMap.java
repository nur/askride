package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="users_role_map")
public class UsersRoleMap extends Model {

  @Id
  @Column(name="user_id")
  public Long user_id;
  
  public String role_code;
  
  public static Finder<Long,UsersRoleMap> find = new Finder<Long,UsersRoleMap>(Long.class, UsersRoleMap.class); 

}
