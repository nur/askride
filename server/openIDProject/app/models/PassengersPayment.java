package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="passengers_payment")
public class PassengersPayment extends Model {

  @Id
  @Column(name="passengers_payment_id")
  public Long passengers_payment_id;
  public String user_name;
  public Long transaction_id;
  public Long ride_id;
  public float fare_amount;
  public float discount_offer;
  public String create_date_time;
  
    
  public static Finder<Long,Passenger> find = new Finder<Long,Passenger>(Long.class, Passenger.class); 

}
