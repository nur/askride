package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@SequenceGenerator(name="Seq", sequenceName="user_id_seq")
@Table(name="users",  schema = "public")
public class User extends Model {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Seq")
  @Column(name="user_id", unique = true, nullable = false)
  public Long user_id;
  

  public String user_name;
  
  public String auth_provider;
  public String password;
  public String first_name;
  public String last_name;
  public String email_id;
  public String phoneno;
  
    
  public static Finder<Long,User> find = new Finder<Long,User>(Long.class, User.class); 
  
  public static User usersByEmail(String email) {
	  return User.find.where().eq("email_id", email).findUnique();
	
  }
  public static User usersByUserName(String userName) {
	  return User.find.where().eq("user_name", userName).findUnique();
	
  }  
}
