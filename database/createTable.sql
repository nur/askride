
CREATE TYPE ride_status AS ENUM ('COMPLETE','DCANCELLED','PCANCELLED');
CREATE TYPE CANCEL_REASONS AS ENUM ('MEDICAL', 'MECHANICAL','ACCIDENT','OTHER');
CREATE TYPE Sattlement_Type AS ENUM ('PENDING', 'COMPLETED');

CREATE TABLE users (
user_id	    SERIAL,
user_name	Varchar(100),
auth_provider	Varchar(30),
password	Varchar(100),
first_name	Varchar(100),
last_name	Varchar(100),
email_id	Varchar(100),
phoneno	    Varchar(12),
pin         Varchar(4),
CONSTRAINT pk_users PRIMARY KEY(user_id)
);

CREATE TABLE role (
code	Varchar(20),
description	Varchar(50),
CONSTRAINT pk_role PRIMARY KEY(code)
);

CREATE TABLE users_role_map (
user_id	INTEGER,
role_code	Varchar(20),
CONSTRAINT pk_users_role_map PRIMARY KEY(user_id, role_code)
);

CREATE TABLE passenger (
user_name	Varchar(100),
credit_card_no	Varchar(30),
exp_date	Varchar(10),
cvv         Varchar(3),
pin         Varchar(4),
total_miles_travelled	INTEGER,
total_bonus	INTEGER,
CONSTRAINT pk_passenger PRIMARY KEY(user_name)
);

CREATE TABLE bonus_payout (
bonus_id	SERIAL,
ride_id	SERIAL,
passenger_id	INTEGER,
bonus_utilized_amount	INTEGER,	
CONSTRAINT pk_bonus_payout PRIMARY KEY(bonus_id, ride_id, passenger_id)
);


CREATE TABLE driver (
driver_id	SERIAL,
user_name	Varchar(100),
dl_no	Varchar(20),	
dl_state	char(2),
dl_issue_date	Date,	
dl_expiration_date	Date,	
dmv_response	Char(1)	,
rating_1	INTEGER	,
rating_2	INTEGER	,
rating_3	INTEGER	,
rating_4	INTEGER	,
rating_5	INTEGER	,
total_earning	NUMERIC(10,2),	
last_payment_received_dttm	TIMESTAMP,	

CONSTRAINT pk_driver PRIMARY KEY(driver_id)
);

CREATE TABLE car (
car_id	SERIAL,
driver_id INTEGER,	
registration_no	Varchar(100),
make	Varchar(50),
model	Varchar(50),
year	INTEGER	,
color	Varchar(20),
car_image	Bytea	,

CONSTRAINT pk_car PRIMARY KEY(car_id)
);


CREATE TABLE ride (
	ride_id	SERIAL,
	driver_id	INTEGER,
	passenger_id	INTEGER,
	car_id	INTEGER,
	passenger_count	INTEGER,
	ride_dttm	TIMESTAMP,
	from_address Text,
	to_address  Text,	
	mile	INTEGER,
	currency	Varchar(25),
	fare	NUMERIC(5,2),
	passenger_earned_bonus	INTEGER,
	driver_rating	INTEGER,
	status	ride_status,
	remark	Text,

CONSTRAINT pk_ride PRIMARY KEY(ride_id, driver_id, passenger_id, car_id)
);

CREATE TABLE driver_cancelled_ride (
	ride_id	INTEGER,
	cancell_dttm	TIMESTAMP,
	cancel_reason	CANCEL_REASONS,
	sattlement_staus	Sattlement_Type,

	CONSTRAINT pk_driver_cancelled_ride PRIMARY KEY(ride_id)
);


CREATE TABLE secondary_passenger_info (
    passenger_id INTEGER,
	ride_id INTEGER,
	secondary_passenger_first_name Varchar(100),
	secondary_passenger_last_name Varchar(100),

    CONSTRAINT pk_secondary_passenger_info PRIMARY KEY(passenger_id, ride_id)
);

CREATE TABLE transaction (
transaction_id     SERIAL,
transaction_mode    VARCHAR(16),
payment_status     VARCHAR(6),
third_party_transaction_id  VARCHAR(20),
transaction_time     TIMESTAMP,
transaction_amonut    NUMERIC(16,2),
transaction_type    VARCHAR(10),
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_transaction PRIMARY KEY(transaction_id)
);

CREATE TABLE drivers_payment (
drivers_payment_id   SERIAL,
driver_id      INTEGER,
transaction_id     INTEGER,
payment_for_period_start_dt DATE,
payment_for_period_end_dt  DATE,
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_drivers_payment PRIMARY KEY(drivers_payment_id)
);

CREATE TABLE drivers_payment_ride_map (
drivers_payment_id   INTEGER,
ride_id      INTEGER,
CONSTRAINT pk_drivers_payment_ride_map PRIMARY KEY(drivers_payment_id,ride_id)
);

CREATE TABLE passengers_payment (
passengers_payment_id  SERIAL,
user_name    Varchar(100),
transaction_id     INTEGER,
ride_id      INTEGER,
fare_amount    NUMERIC(16,2),
discount_offer    NUMERIC(16,2),
create_date_time    TIMESTAMP,
CONSTRAINT pk_passengers_payment PRIMARY KEY(passengers_payment_id)
);


CREATE TABLE insurance (
insurance_id      INTEGER, /* Ride_Id will be treated as our internal Insurance ID*/
third_party_insurance_id  VARCHAR(20),
premium_amonut     NUMERIC(16,2),
coverage_amonut    NUMERIC(16,2),
num_of_person_covered  INTEGER,
clock_start_time     TIMESTAMP,
clock_end_time      TIMESTAMP,
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_insurance PRIMARY KEY(insurance_id)
);


CREATE TABLE ref_promotion (
code        VARCHAR(20),
description     VARCHAR(100),
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_ref_promotion PRIMARY KEY(code)
);

CREATE TABLE promotional_offer (
promotional_offer_id  SERIAL,
passenger_id      INTEGER,
promotion_code     VARCHAR(20),
promotion_date_time   TIMESTAMP,
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_promotional_offer PRIMARY KEY(promotional_offer_id)
);


CREATE TABLE gps_data (
driver_id     INTEGER,
x_coordinate      VARCHAR(100),
y_coordinate     VARCHAR(100),
CONSTRAINT pk_gps_data PRIMARY KEY(driver_id)
);



CREATE TABLE accident (
accident_id      INTEGER,
ride_id       INTEGER,
accident_date_time    TIMESTAMP,
accident_location    VARCHAR(200),
details      VARCHAR(400),
created_by     VARCHAR(100),
created_time    TIMESTAMP,
updated_by     VARCHAR(100),
updated_time    TIMESTAMP,
CONSTRAINT pk_accident PRIMARY KEY(accident_id)
);

CREATE TABLE claim (
claim_id      SERIAL,
accident_id    INTEGER,
status      VARCHAR(20),
claim_amonut    NUMERIC(16,2),
settlement_amonut   NUMERIC(16,2),
created_by    VARCHAR(100),
created_time   TIMESTAMP,
updated_by    VARCHAR(100),
updated_time   TIMESTAMP,
CONSTRAINT pk_claim PRIMARY KEY(claim_id)
);

CREATE TABLE user_location (
user_name Varchar(100),
longitude Varchar(50),
latitude Varchar(50),
user_type char(1),
active_ind char(1)
);

CREATE TABLE state (
state_name VARCHAR(50),
state      CHAR(2)
);
