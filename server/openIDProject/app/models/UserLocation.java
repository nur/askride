package models;

import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="user_location",  schema = "public")
public class UserLocation extends Model {

  @Id
  
  @Column(name="user_name", unique = true, nullable = false)
  public String user_name;
  public String longitude;
  public String latitude;  
  public char user_type;
  public char active_ind;
  
    
  public static Finder<Long,UserLocation> find = new Finder<Long,UserLocation>(Long.class, UserLocation.class); 
  
  public static UserLocation findAllUserLocationByUserName(String userName) {
	  return UserLocation.find.where().eq("user_name", userName).findUnique();
	
  }
}
