# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table bonus_payout (
  bonus_id                  bigint not null,
  ride_id                   bigint,
  passenger_id              bigint,
  bonus_utilized_amount     bigint,
  constraint pk_bonus_payout primary key (bonus_id))
;

create table car (
  car_id                    bigint not null,
  driver_id                 bigint,
  registration_no           varchar(255),
  make                      varchar(255),
  model                     varchar(255),
  year                      integer,
  color                     varchar(255),
  car_image                 bytea,
  constraint pk_car primary key (car_id))
;

create table driver (
  driver_id                 bigint not null,
  dl_no                     varchar(255),
  dl_state                  varchar(255),
  dl_issue_date             varchar(255),
  dl_expiration_date        varchar(255),
  dmv_response              varchar(255),
  rating_1                  bigint,
  rating_2                  bigint,
  rating_3                  bigint,
  rating_4                  bigint,
  rating_5                  bigint,
  total_earning             float,
  last_payment_received_dttm varchar(255),
  constraint pk_driver primary key (driver_id))
;

create table passenger (
  user_name                 varchar(255) not null,
  credit_card_no            varchar(255),
  exp_date                  varchar(255),
  cvv                       varchar(255),
  pin                       varchar(255),
  total_miles_travelled     bigint,
  total_bonus               bigint,
  constraint pk_passenger primary key (user_name))
;


create table passengers_payment (
  passengers_payment_id     bigint not null,
  user_name                 varchar(255),
  transaction_id            bigint,
  ride_id                   bigint,
  fare_amount               float,
  discount_offer            float,
  create_date_time          varchar(255),
  constraint pk_passengers_payment primary key (passengers_payment_id))
;

create table role (
  code                      varchar(255) not null,
  user_name                 varchar(255),
  description               varchar(255),
  constraint pk_role primary key (code))
;

<<<<<<< HEAD
create table state (
  state_name                varchar(255) not null,
  state                     varchar(255),
  constraint pk_state primary key (state_name))
;

=======
>>>>>>> my-modifications
create table public.users (
  user_id                   bigint not null,
  user_name                 varchar(255),
  auth_provider             varchar(255),
  password                  varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  email_id                  varchar(255),
  phoneno                   varchar(255),
  constraint pk_users primary key (user_id))
;

create table public.user_location (
  user_name                 varchar(255) not null,
  longitude                 varchar(255),
  latitude                  varchar(255),
  user_type                 varchar(255),
  active_ind                varchar(255),
  constraint pk_user_location primary key (user_name))
;

create table users_role_map (
  user_id                   bigint not null,
  role_code                 varchar(255),
  constraint pk_users_role_map primary key (user_id))
;

create sequence bonus_payout_seq;

create sequence car_seq;

create sequence driver_seq;

create sequence passenger_seq;

create sequence passengers_payment_seq;

create sequence role_seq;

create sequence state_seq;

create sequence usertypes_usertypeid_seq;

create sequence public.user_location_seq;

create sequence users_role_map_seq;
<<<<<<< HEAD
=======


>>>>>>> my-modifications




# --- !Downs

<<<<<<< HEAD
drop table if exists bonus_payout cascade;

drop table if exists car cascade;

drop table if exists driver cascade;

drop table if exists passenger cascade;

drop table if exists passengers_payment cascade;

drop table if exists role cascade;

drop table if exists state cascade;

=======
drop table if exists role cascade;

>>>>>>> my-modifications
drop table if exists public.users cascade;

drop table if exists public.user_location cascade;

drop table if exists users_role_map cascade;

drop sequence if exists bonus_payout_seq;

drop sequence if exists car_seq;

drop sequence if exists driver_seq;

drop sequence if exists passenger_seq;

drop sequence if exists passengers_payment_seq;

drop sequence if exists role_seq;

drop sequence if exists state_seq;

drop sequence if exists usertypes_usertypeid_seq;

drop sequence if exists public.user_location_seq;

drop sequence if exists users_role_map_seq;

