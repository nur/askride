package models;


import javax.persistence.*;

import play.db.ebean.*;

@Entity 
@Table(name="state")
public class State extends Model {

	@Id
	@Column(name="state_name")
	public String state_name;
	public String state;  

	public static Finder<Long,State> find = new Finder<Long,State>(Long.class, State.class); 

}

