package controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import models.Driver;
import models.User;
import models.UserLocation;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Result;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.core.Identity;
import securesocial.core.java.SecureSocial;
import views.html.*;



public class DriverController extends Controller{

	private static final int SUCCESS=0;
	private static final int FAILURE=1;
    /*
     * Example POST request ->
     * 
     * curl -X POST -H "Content-Type: application/json" -d " { \"user_name\":\"686023461494046\", \"first_name\":\"Palak\", \"last_name\":\"Debnath\", \"phone_no\":\"2244001301\", \"dl_no\":\"D1531210\", \"dl_state\":\"IL\", \"dl_issue_date\":\"02-SEP-11\", \"dl_expiration_date\":\"02-SEP-16\"}" http://localhost:9000/saveDriverInfo
     * 
     */
	public static Result saveDriverInfo() {
		
		JsonNode val = request().body().asJson();
		ObjectNode result = Json.newObject();

		// System.out.println(val);
		String user_name=val.findPath("user_name").textValue();
		String first_name = val.findPath("first_name").textValue();
		String last_name = val.findPath("last_name").textValue();
		String phone_no   = val.findPath("phone_no").textValue();
				
		String dl_no =val.findPath("dl_no").textValue();
		String dl_state=val.findPath("dl_state").textValue();
		String dl_issue_date=val.findPath("dl_issue_date").textValue();
		String dl_expiration_date=val.findPath("dl_expiration_date").textValue();

		System.out.println("user_name = " + user_name);
		System.out.println("dl_issue_date = " + dl_issue_date);
		System.out.println("dl_expiration_date = " + dl_expiration_date);
		
        //Update User details first
		User user = User.usersByUserName(user_name);
		user.first_name = first_name;
		user.last_name = last_name;
		user.phoneno = phone_no;
		user.save();
		
		System.out.println("User Info Saved!");

		//Save Driver details now
		Driver driver = new Driver();
		driver.user_name = user_name;
	    driver.dl_no = dl_no;
	    driver.dl_state = dl_state;
	    
	    DateFormat format = new SimpleDateFormat("DD-MM-YY");
	    Date dl_issue_dt = null;
	    Date dl_expiration_dt = null;
	    
	    try {
			dl_issue_dt = format.parse(dl_issue_date);
		    dl_expiration_dt = format.parse(dl_expiration_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		driver.dl_issue_date = dl_issue_dt;
	    driver.dl_expiration_date = dl_expiration_dt;
	    
	    driver.save();
		
		System.out.println("Driver data saved...");
		result.put("status",SUCCESS); //0-> Success    1->Failure
		result.put("message","Driver info Added Successfully"); 

		return ok(result);
	}
	
	public static Result makeDriverAvailable(String userName) {

	    JsonNode val = request().body().asJson();
	    ObjectNode result = Json.newObject();

	    UserLocation userLoc = UserLocation.findAllUserLocationByUserName(userName);  //.findByID(userName)
	    userLoc.active_ind = 'Y';
	    userLoc.save();

	    result.put("status",SUCCESS); //0-> Success    1->Failure
	    result.put("message","Driver made available Successfully");
	    return ok(result);
	}

	public static Result makeDriverNotAvailable(String userName) {

	    JsonNode val = request().body().asJson();
	    ObjectNode result = Json.newObject();

	    UserLocation userLoc = UserLocation.findAllUserLocationByUserName(userName);
	          userLoc.active_ind = 'N';
	    userLoc.save();

	    result.put("status",SUCCESS); //0-> Success    1->Failure
	    result.put("message","Driver made NOT available Successfully");
	    return ok(result);
	}

}

